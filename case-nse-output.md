#### Application
*where does the data come from? what is the data used for? Like simulation or data analysis? is there documentation or a paper/preprint available, ...?*
 
 * output tracking for linearized Navier-Stokes equations
 * forward simulation data
 * backward simulation and differential Riccati equations
 * paper: [DOI: 10.1137/151004963](https://doi.org/10.1137/151004963)

#### Structure
*time series, space-discretizations, tensors/matrices as operators/data fields, factorizations, approximations, hierarchies, is there a mathematical model description, ...?*

 * forward time series data
 * Riccati-matrix time series data (factorized, approximated)
 * approximates the solution to a mathematical model

#### Quantities
*how much, large files, many files, static, volatile, ...?*

 * not static -- every new setup will generate/require new data
 * can be arbitrarily large and arbitrarily many files
 * for the reported academical example (driven cavity): 
	 * timesteps * (forward + backward + Riccati)
	 * 128 * (2*0.04MB + 1.9MB) = 250MB
 * 2D cylinder wake: 
	 * time steps: 2500
	 * one forward state: 1MB
	 * one factorized Riccati solution: 360MB
     * estimated storage requirement for a similar simulation 1.8TB
 * 3D cylinder wake
	 * forward state: 7.5MB

#### Online/Offline/Post Processing
*data needed all at once vs. parts needed at runtime, not all data needed but derivatives like mean values, maximum values*

 * to recompute the feedback, at runtime the corresponding data points are needed
 * to redo the simulation or to reuse the feedback law, only the *gains* are needed (can be computed from the Riccati solution, are significantly smaller)

#### Accessibility
Please state how your data is accessible and what accessibility might be useful for your project.
*accessible to coworkers/the public, findable/searchable, can be modified/extended, can be accessed in parts, can be post processed*

 * as of now: the data can be shared/accessed in one chunk
 * useful: quick access of certain data points
 * useful: missing data points can be computed/interpolated on demand
 * useful: post processing for efficient visualization and examination

#### Further Issues

