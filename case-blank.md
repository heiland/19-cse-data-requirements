Dear Boris,

there is a huge German project [1] to set up infrastructure for research data. Our CSC group will be part of the consortium that wants to take care of the mathematics branch. 

As of now, we try to figure out the potential needs of computational science engineering (CSE) through analysing use cases. 

Can you give me some basic information (key words are sufficient) of one particular or the whole zoo of your projects regarding the structure and the generation, processing, loading, and storing of data. Below, I have put down a few points. Please indicate how your data/projects fits these categories. Surely, I have missed important features of your applications. Please add them under *further issues*.

Would be great if you could provide me with some insights by the end of this week. Whenever you have questions, please write me. 

Best regards from Beijing and a happy Chinese new year,
Jan


#### Application
*where does the data come from? what is the data used for? Like simulation or data analysis? is there documentation or a paper/preprint available, ...?*

#### Structure
*time series, space-discretizations, tensors/matrices as operators/data fields, factorizations, approximations, hierarchies, is there a mathematical model description, ...?*

#### Quantities
*how much, large files, many files, static, volatile, ...?*

#### Online/Offline/Post Processing
*data needed all at once vs. parts needed at runtime, not all data needed but derivatives like mean values, maximum values*

#### Accessibility
Please state how your data is accessible and what accessibility might be useful for your project.
*accessible to coworkers/the public, findable/searchable, can be modified/extended, can be accessed in parts, can be post processed*

#### Further Issues



Thanks a lot!


[1]: https://www.dfg.de/en/service/press/press_releases/2018/press_release_no_58/index.html
