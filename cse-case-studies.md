# CSE Case Studies

We collect aspects, issues, and demands of the generation and processing of data in CSE.

## Example Application Areas
*where does the data come from? what is the data used for? Like simulation or data analysis? is there documentation or a paper/preprint available, ...?*
	
 * **[1]**: Optimization/output tracking of incompressible fluid flow: [DOI: 10.1137/151004963](https://doi.org/10.1137/151004963)
 * **[2]**: Simulation/model reduction of power cells: [DOI: 10.1007/978-3-319-75538-0_18](https://doi.org/10.1007/978-3-319-75538-0_18)
 * **[3]**: Computing electrostatic potentials: [arXiv: 1901.09864](https://arxiv.org/abs/1901.09864)

## Structure
*time series, space-discretizations, tensors/matrices as operators/data fields, factorizations, approximations, hierarchies, is there a mathematical model description, ...?*

 * time series data, **[1]**
   * needed to compute backwards quantities of interest
	 * here, Riccati-matrix time series data 
 * hierarchical approximation layers, **[2]**
 * tensors/matrices as operators (factorized, approximated), **[1]**, **[2]**, **[3]**
 * mathematical models are available, here discretizations of PDEs, **[1]**, **[2]**, **[3]**

## Quantities
*how much, large files, many files, static, volatile, ...?*

 * not static -- every new setup will generate/require new data, **[1]**
 * static -- compute entries for a database **[3]**
 * in relevant examples the data easily excessess the capacities of PCs, **[1]**, **[2]**, **[3]**
   * **[1]**: reported example needs ~250MB storage, estimated storage requirement for a more realistic example (2D cylinder wake): ~1.8TB
   * **[2]**:
   * **[3]**: 3D computations, with reduction and on a 257*257*257 tensor grid: ~135MB

## Online/Offline/Post Processing
*data needed all at once vs. parts needed at runtime, not all data needed but derivatives like mean values, maximum values*

 * at runtime specific data points **[1]** or operators (here: the *nonlinerities*) **[2]** are needed
 * for the startup, all data/operators are needed once **[2]**
 * in certain cases, derivations of the data are needed (here: the *gains*) **[1]** 

## Accessibility
*accessible to coworkers/the public, findable/searchable, can be modified/extended, can be accessed in parts, can be post processed*

 * possibly: data or critical parts must not be shared or only within a group **[2]**
 * as of now: the data can be shared/accessed in one chunk **[1]**, **[3]**
 * useful: quick access of certain data points **[1]**
 * useful: missing data points can be computed/interpolated on demand **[1]**
 * useful: post processing for efficient visualization and examination **[1]**, **[2]**, **[3]**

